package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        assertEquals("Add", 9, new Calculator().add());
        
    }
    
    @Test
    public void testSub() throws Exception {

        assertEquals("Sub", 3, new Calculator().sub());

    }
}
